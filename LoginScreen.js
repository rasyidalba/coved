import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, Button, Image } from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";


export default class LoginScreen extends React.Component{
constructor(props) {
    super(props)
    this.state = {
    userName: '',
    password: '',
    isError: false,
     }
}

loginHandler(){
    console.log(this.state.userName, " ", this.state.password)

    if(this.state.password == 'stayathome'){
        this.props.navigation.navigate("Home", {
            userName : this.state.userName
        })
    }
    else{
        this.setState({isError : true})
    }
    
}

render(){
    return (
        <View style={styles.container}>
            <Text style={{color:'white', alignSelf:'center', fontSize:22}}>Cov<Text style={{fontSize:22, color:'black', alignSelf:'center'}}>Ed</Text>
            </Text>

            <Image source={require('./HomeTab/assets/Logo.png')}  style={{marginTop:20, height:150,width:150, borderRadius:75, alignSelf:'center'}}/>

            <View style={styles.userBox}>
                <Text>Username</Text>
                <View>
                    <TextInput
                    style={styles.inputBox}
                    onChangeText={userName => this.setState({ userName })}
                    />
                </View>
            </View>
            
            <View style={styles.passBox}>
                <Text>Password</Text>
                <View>
                    <TextInput
                    style={styles.inputBox}
                    onChangeText={password => this.setState({ password })}
                    secureTextEntry={true}
                    />
                    <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Wrong Password!</Text>
                </View>
            </View>

            <View style={styles.passBox}>
                <Button title='Login' style={styles.loginBtn}
                 onPress={() => this.loginHandler()} />
            </View>
    

        </View>


    )
}

    
};

const styles = StyleSheet.create({
  container:{
    marginTop:40,
    flex:1,
    backgroundColor: '#136A8A'
  },
  username:{
    backgroundColor:"green",

    borderRadius:10

  },
  userBox:{
    marginTop:100,
    alignSelf:'center',
    width: 219,
    height: 46,
    // backgroundColor:'gold'
  },
  passBox:{
    marginTop:40,
    alignSelf:'center',
    width: 219,
    height: 46,
    
  },
  inputBox:{
    height:30,
    width:219,
    backgroundColor:'#C4C4C4',
    borderRadius:10
  },
  textInput: {
    width: 219,
    backgroundColor: 'white'
  },
  loginBtn:{
    borderRadius:10
    
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }

})

