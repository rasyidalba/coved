import React, { Component } from 'react';
import {
    FlatList,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { StatusBar } from 'expo-status-bar';
import { createStore } from 'redux'
import {provider} from 'react-redux'

import LoginScreen from './LoginScreen'
import Tab from './HomeTab'


const RootStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();
const Draw = createDrawerNavigator();



export default class flatlists extends Component {

    render(){
        return (
            <NavigationContainer>
                    <RootStack.Navigator>
                        <RootStack.Screen name="Login" component={LoginScreen} />
                        <RootStack.Screen name="Home" component={Tab} />
                    </ RootStack.Navigator>
            </NavigationContainer>
              

        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F5FCFF'
    }
})
