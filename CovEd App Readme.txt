Aplikasi ini dibuat guna menyelesaikan Final Project React Native Sanbercode yang dibuat oleh
Rasyid Aulia Alba dan Whraspathi Yatindra Jannata, aplikasi ini digunakan untuk melihat secara persebaran Covid-19 di Indonesia.

Data yang tersedia kami peroleh dari API

https://indonesia-covid-19-api.now.sh/api
https://indonesia-covid-19.mathdro.id/api/provinsi

Untuk melakukan akses di kolom password hanya dapat diisi dengan stayathome