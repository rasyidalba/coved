import React, { Component } from 'react';
import {
    FlatList,
    StyleSheet,
    Text,
    View,
    Dimensions
} from 'react-native';
import {

  LineChart,

  BarChart,

  PieChart,

  ProgressChart,

  ContributionGraph,

  StackedBarChart

} from "react-native-chart-kit";
import { StatusBar } from 'expo-status-bar';
import { ScrollView } from 'react-native-gesture-handler';

export default class Home extends Component {

    state = {
        data: []
    }

    componentDidMount(){
        this.fetchdata();
    }

    fetchdata = async () => {
        const response = await fetch("https://indonesia-covid-19-api.now.sh/api");
        const json = await response.json();
        this.setState({data: json})
    }

    render(){
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Text style={styles.homeTitle} >Kasus Covid-19 di Indonesia</Text>
                    <Text style={styles.caseTitle}>Kasus Positif</Text>
                    <Text style={styles.numCase}> {`${this.state.data.jumlahKasus}`} </Text>
                    <View style={styles.detailCaseContainer}>
                        <View style={styles.detailCase}>
                            <Text style={styles.caseTitle}>Kasus Sembuh</Text>
                            <Text style={styles.numCaseSembuh}> {`${this.state.data.sembuh}`} </Text>
                        </View>
                        <View style={styles.detailCase}>
                            <Text style={styles.caseTitle}>Kasus Meninggal</Text>
                            <Text style={styles.numCaseMenimggal}> {`${this.state.data.meninggal}`} </Text>
                        </View>
                    </View>  
                    <Text style={styles.homeTitle}>Penambahan Kasus Positif {`\n`} di Indonesia</Text>
                    <LineChart
                    data={{
                        labels: ['Feb','Mar 30' ,'Apr 27', 'Mei 25', 'Jun 29'],
                        datasets: [
                            {
                                data:[0,4546,8919,15281,35669]
                            }
                        ]
                    }}
                    width={Dimensions.get("window").width-50}
                    height={200}
                    chartConfig={{
                        backgroundGradientFrom: "#F5FCFF",
                        backgroundGradientTo: "#F5FCFF",
                        color: (opacity = 1) => 'black',
                        decimalPlaces:0,
                        
                    }}
                    /> 
                    </ScrollView>  
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingVertical:30,
        paddingHorizontal:20,
        backgroundColor: '#136A8A'
    },
    homeTitle:{
        fontSize: 20,
        fontWeight:'bold',
        paddingTop:20,
        paddingBottom: 35,
        alignSelf:'center',
        textAlign:'center'
    },
    caseTitle:{
        fontSize: 14,
        paddingTop:30,
        alignSelf:'center'
    },
    numCase:{
        fontSize:68,
        fontWeight:'bold',
        alignSelf:'center'
    },
    numCaseSembuh:{
        fontSize:40,
        fontWeight:'bold',
        color:'#40B40A',
        paddingBottom:25
    },
    numCaseMenimggal:{
        fontSize:40,
        fontWeight:'bold',
        paddingBottom:25,
        color: '#F40000'
    },
    detailCaseContainer:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    detailCase:{
        alignItems:'center'
    }
})
