import React, { Component } from "react";
import { View, Text, TextInput, StyleSheet, Button, Image, Dimensions } from 'react-native';


const Screen = Dimensions.get('window')

export default class About extends React.Component{
 render(){
    
    return( 
        <View style={styles.container}>
            <Image source={require('./assets/accIcon.png')} style={{height:100, width:100, alignSelf:'center', borderRadius: 50}}/>

            <Text style={{position:'absolute', width: 99, height: 36, left: 18, top: 152, fontSize:22}}>Hi, user!</Text>

            <View style={styles.aboutBox}>
                <View style={styles.textBox}>
                    <Text style={{fontSize:18}}>
                    CovEd adalah aplikasi mobile yang bertujuan untuk memberikan info kepada user mengenai perkembangan kasus corona virus di Indonesia secara real-time setiap hari, dengan detail kasus di tiap provinsi.{"\n"}{"\n"}
                    Aplikasi ini juga menyediakan info mengenai rumah sakit darurat corona virus yang berada di Yogyakarta. 
                    </Text>
                </View>

            </View>

        </View>

    )
    
}

}

const styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop:40,
        backgroundColor: '#136A8A'
    },
    aboutBox:{
        position: "absolute",
        width: Screen.width,
        height: Screen.height,
        left: 0,
        top: 203,

        backgroundColor: '#D3E4FF',
        borderTopLeftRadius:12,
        borderTopRightRadius:12  

    },
    textBox:{
        marginTop:40,
        marginLeft:15,
        height:300,
        width:300,
        
    }


}
)