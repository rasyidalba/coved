import React, { Component } from "react";
import { StyleSheet, Text, View, FlatList, Button } from "react-native"
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";



export default class App extends Component {

    render() {
        return (
            <View style={StyleSheet.container}>
                
                    <ScrollView>
                        <Button  title= 'Jawa Timur' onPress={(item) => this.props.navigation.navigate('Detail Jatim') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'DKI Jakarta' onPress={(item) => this.props.navigation.navigate('Detail DKI Jakarta') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Sulawesi Selatan' onPress={(item) => this.props.navigation.navigate('Detail Sulawesi Selatan') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Jawa Tengah' onPress={(item) => this.props.navigation.navigate('Detail Jawa Tengah') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Jawa Barat' onPress={(item) => this.props.navigation.navigate('Detail Jawa Barat') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Kalimantan Selatan' onPress={(item) => this.props.navigation.navigate('Detail Kalimantan Selatan') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Sumatera Selatan' onPress={(item) => this.props.navigation.navigate('Detail Sumatera Selatan') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Papua' onPress={(item) => this.props.navigation.navigate('Detail Papua') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Bali' onPress={(item) => this.props.navigation.navigate('Detail Bali') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Sumatera Utara' onPress={(item) => this.props.navigation.navigate('Detail Sumatera Utara') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Banten' onPress={(item) => this.props.navigation.navigate('Detail Banten') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Nusa Tenggara Barat' onPress={(item) => this.props.navigation.navigate('Detail Nusa Tenggara Barat') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Sulawesi Utara' onPress={(item) => this.props.navigation.navigate('Detail Sulawesi Utara') }   />
                        <View style={styles.border}></View>
                        <Button  title= 'Kalimantan Tengah' onPress={(item) => this.props.navigation.navigate('Detail Kalimantan Tengah') }   />
                    </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F5FCFF',
        marginVertical:30,
    },
    flatlistContainer:{
        height:35,
        marginLeft:10,
        marginRight:10,
        backgroundColor:'red',
        alignSelf: 'stretch',
        flexDirection:'column',
        justifyContent: 'space-around',
        paddingTop:10
    },
    buttonStyle:{
        color: 'red',
        height:35,
        marginLeft:10,
        marginRight:10,
    },
    border:{
        height : 10,
        backgroundColor:'white'
    }
})