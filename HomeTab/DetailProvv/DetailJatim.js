import React, { Component } from 'react';
import {
    FlatList,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { StatusBar } from 'expo-status-bar';
import Icon from 'react-native-vector-icons/MaterialIcons';


const getByValue2 = (arr, value) => {

    for (var i=0, iLen=arr.length; i<iLen; i++) {
      if (arr[i].provinsi == value) return arr[i].kasusPosi;
    }
}

const getByValue3 = (arr, value) => {

    for (var i=0, iLen=arr.length; i<iLen; i++) {
      if (arr[i].provinsi == value) return arr[i].kasusSemb;
    }
}

const getByValue4 = (arr, value) => {

    for (var i=0, iLen=arr.length; i<iLen; i++) {
      if (arr[i].provinsi == value) return arr[i].kasusMeni;
    }
}


export default class flatlists extends Component {
    state = {
        data: []
    }
    componentDidMount(){
        this.fetchdata();
    } 
    fetchdata = async () => {
        const response = await fetch("https://indonesia-covid-19.mathdro.id/api/provinsi");
        const json = await response.json();
        this.setState({data: json.data})
    }
    render(){
        
        return (
            <View style={styles.container}>
                        <Text style={styles.caseTitle}>Kasus Positif</Text>
                        <Text style={styles.numCase}> {`${getByValue2(this.state.data, "Jawa Timur")}`} </Text>
                        <View style={styles.detailCaseContainer}>
                            <View style={styles.detailCase}>
                                <Text style={styles.caseTitle}>Kasus Sembuh</Text>
                                <Text style={styles.numCaseSembuh}> {`${getByValue3(this.state.data, "Jawa Timur")}`} </Text>
                            </View>
                            <View style={styles.detailCase}>
                                <Text style={styles.caseTitle}>Kasus Meninggal</Text>
                                <Text style={styles.numCaseMenimggal}> {`${getByValue4(this.state.data, "Jawa Timur")}`} </Text>
                            </View>
                        </View>
            </View>
)}}


const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#F5FCFF',
        justifyContent :'center'
    },
    homeTitle:{
        fontSize: 20,
        fontWeight:'bold',
        paddingTop:20,
        paddingBottom: 35,
        alignSelf:'center'
    },
    caseTitle:{
        fontSize: 14,
        paddingTop:30,
        alignSelf:'center'
    },
    numCase:{
        fontSize:68,
        fontWeight:'bold',
        alignSelf:'center'
    },
    numCaseSembuh:{
        fontSize:40,
        fontWeight:'bold',
        color:'#40B40A'
    },
    numCaseMenimggal:{
        fontSize:40,
        fontWeight:'bold',
        color: '#F40000'
    },
    detailCaseContainer:{
        flexDirection:'row',
        justifyContent: 'space-around'
    },
    detailCase:{
        alignItems:'center'
    }
})
