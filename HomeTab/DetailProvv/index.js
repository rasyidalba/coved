import React, { Component } from 'react';
import {
    FlatList,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { StatusBar } from 'expo-status-bar';


import ListProvinsi2 from './flatlistAPIcoba'
import DetailJatim from './DetailJatim'
import DetailJak from './DetailJakarta'
import DetailSulsel from './DetailSulsel'
import DetailJateng from './DetailJateng'
import DetailJabar from './DetailJabar'
import DetailKalsel from './DetailKalsel'
import DetailSumsel from './DetailSumsel'
import DetailPapua from './DetailPapua'
import DetailBali from './DetailBali'
import DetailSumut from './DetailSumut'
import DetailBanten from './DetailBanten'
import DetailNTB from './DetailNTB'
import DetailSulut from './DetailSulut'
import DetailKalteng from './DetailKalteng'


const RootStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();
const Draw = createDrawerNavigator();

export default class flatlists extends Component {

    render(){
        return (
                
                    <RootStack.Navigator>
                        <RootStack.Screen name="Provinsi dengan Kasus Tertinggi" component={ListProvinsi2} />
                        
                        <RootStack.Screen name="Detail Jatim" component={DetailJatim} />
                        <RootStack.Screen name="Detail DKI Jakarta" component={DetailJak} />
                        <RootStack.Screen name="Detail Sulawesi Selatan" component={DetailSulsel} />
                        <RootStack.Screen name="Detail Jawa Tengah" component={DetailJateng} />
                        <RootStack.Screen name="Detail Jawa Barat" component={DetailJabar} />
                        <RootStack.Screen name="Detail Kalimantan Selatan" component={DetailKalsel} />
                        <RootStack.Screen name="Detail Sumatera Selatan" component={DetailSumsel} />
                        <RootStack.Screen name="Detail Papua" component={DetailPapua} />
                        <RootStack.Screen name="Detail Bali" component={DetailBali} />
                        <RootStack.Screen name="Detail Sumatera Utara" component={DetailSumut} />
                        <RootStack.Screen name="Detail Banten" component={DetailBanten} />
                        <RootStack.Screen name="Detail Nusa Tenggara Barat" component={DetailNTB} />
                        <RootStack.Screen name="Detail Sulawesi Utara" component={DetailSulut} />
                        <RootStack.Screen name="Detail Kalimantan Tengah" component={DetailKalteng} />
                    </ RootStack.Navigator>
                    
              

        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F5FCFF'
    }
})
