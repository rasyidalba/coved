import React, { Component } from "react";
import { StyleSheet, Text, View, FlatList } from "react-native"
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";

export default class App extends Component {
    state = {
        data : [{nama : 'RSU Dr. Sardjito',
                alamat : 'Jl. Kesehatan No. 1, Sekip, Sinduadi Yogyakarta, Daerah Istimewa Yogyakarta ',
                noTelp : '(0274) 631190, 587333'},
                {nama : 'Instalasi Radiologi RS Kota Yogyakarta',
                alamat : 'Sorosutan, Umbulharjo, Yogyakarta City, Special Region of Yogyakarta',
                noTelp : '-'},
                {nama : 'RS Wirosaban',
                alamat : 'Jl. Dr. Wahidin Sudirohusodo, Bantul, Daerah Istimewa Yogyakarta',
                noTelp : '0274-367386, 2810721'},
                {nama : 'RSUD Wates Kabupaten Kulon Progo',
                alamat : 'Jl.Tentara Pelajar Km1, Kulon Progo, Daerah Istimewa Yogyakarta',
                noTelp : '(0274) 773169'},
                {nama : 'RS Wirosaban',
                alamat : 'Jl. Taman Bakti No.6, Purbosari, Wonosari, Gunung Kidul, Daerah Istimewa Yogyakarta',
                noTelp : '(0274) 391288'}
            ]
    }

    render() {
        return (
            <View style={StyleSheet.container}>
                <View style ={{height:40}} ></View>
                
                <FlatList 
                data={this.state.data}
                keyExtractor={(x,i) => i}
                ItemSeparatorComponent={()=><View style={{height:10,backgroundColor:"white"}}/>}
                renderItem={({item}) => 
                    <View style={styles.flatlistContainer}>
                        <Text style={styles.hospitalTitle}> {`${item.nama}`}</Text>
                        <Text style={styles.hospitalAddr}> {`${item.alamat}`}</Text>
                        <Text style={styles.hospitalTelp}> {`${item.noTelp}`}</Text>
                    </View>
                }
            />
            
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: '#136A8A',
        flexDirection:'column',
        justifyContent:'space-between',
        alignItems:'stretch',
        marginVertical:50,
    },
    flatlistContainer: {
        height:100,
        marginLeft:10,
        marginRight:10,
        backgroundColor:'#93BADD',
        alignSelf: 'stretch',
        flexDirection:'column',
        justifyContent: 'space-around',
        borderRadius:10
    },
    hospitalTitle:{
        fontSize:20,
        fontWeight:'bold'
    },
    hospitalAddr:{
        color: '#555555',
        fontSize:12

    },
    hospitalTelp:{
        fontSize:14,
        color: '#333333'
    }
})