import React, { Component } from 'react';
import {
    FlatList,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { StatusBar } from 'expo-status-bar';

import DetailProv from './DetailProvv'
import Home from './Home'
import Hospital from './Hospital'
import About from './About'

const RootStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();

export default class flatlists extends Component {

    render(){
        return (
                
                    <Tabs.Navigator>
                        <Tabs.Screen name="Stack" component={Home} />
                        <Tabs.Screen name="Hospital" component={Hospital} />
                        <Tabs.Screen name="List Provinsi" component={DetailProv} />
                        <Tabs.Screen name="About Us" component={About} />
                    </Tabs.Navigator>
                    
                

        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F5FCFF'
    }
})
